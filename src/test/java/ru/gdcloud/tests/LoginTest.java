package ru.gdcloud.tests;

import ru.gdcloud.config.ConfigProperties;
import ru.gdcloud.pages.LoginPage;
import ru.gdcloud.pages.MainPage;
import io.qameta.allure.Description;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class LoginTest {
    public static LoginPage loginPage;
    public static MainPage mainPage;
    public static WebDriver driver;

    public static WebDriver chromeDriver() {
        System.setProperty("webdriver.chrome.driver", ConfigProperties.getProperty("chromedriver"));
        return driver = new ChromeDriver();
    }
    public static WebDriver firefoxDriver() {
        System.setProperty("webdriver.gecko.driver", ConfigProperties.getProperty("geckodriver"));
        return driver = new FirefoxDriver();
    }
    public static WebDriver edgeDriver() {
        System.setProperty("webdriver.edge.driver", ConfigProperties.getProperty("edgedriver"));
        return driver = new EdgeDriver();
    }
    @BeforeClass
    public static void setup() {

        String browserName = System.getProperty("browser_name");
        try {
            if (browserName.equals("chrome")) chromeDriver();

            else if (browserName.equals("edge")) edgeDriver();

            else if (browserName.equals("firefox")) firefoxDriver();
        } catch (NullPointerException npe) {
            chromeDriver();
        }

        loginPage = new LoginPage(driver);
        mainPage = new MainPage(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(ConfigProperties.getProperty("loginpage"));
    }

    @DataProvider(name = "test")
    public static Object[][] usernamesPasswords() {
        return new Object[][]{{ConfigProperties.getProperty("loginName1"), ConfigProperties.getProperty("password1")},
                {ConfigProperties.getProperty("loginName3"), ConfigProperties.getProperty("password3")}};
    }

    @Test(description = "GreenData login page",
            dataProvider = "test")
    @Description("Login")
    public void loginTest(String str1, String str2) {
        //Check for empty input
        loginPage.clickLoginBtn();
        //If input is empty, login doesn't happen and the following lines should be executed correctly
        loginPage.inputLogin(str1);
        loginPage.inputPassword(str2);
        //Checking checkbox
        loginPage.clickRememberCheckBox();
        //Checking login with another account
        loginPage.clickLoginAnotherBtn();
        loginPage.wrongInput();
        //Checking login with current account
        loginPage.clickLoginCurrentBtn();
        loginPage.wrongInput();
        //Checking login button
        loginPage.clickLoginBtn();
        //If username/password a correct
        if (str1.equals("tester")) {
            Assert.assertTrue(mainPage.getUserName().contains(ConfigProperties.getProperty("username")));
            mainPage.dropDownMenu();
            mainPage.logout();
        }
        //If username/password a incorrect
        else  {
            loginPage.wrongInput();
        }
    }

        @AfterClass
        public static void tearDown () {
            driver.quit();
        }
    }


