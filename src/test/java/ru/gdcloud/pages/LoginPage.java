package ru.gdcloud.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    public WebDriver driver;
    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(id = "username")
    private WebElement loginField;

    @FindBy(id = "login_button")
    private WebElement loginBtn;

    @FindBy(id = "login_button_domain")
    private WebElement loginAnotherBtn;

    @FindBy(id = "login_button_current")
    private WebElement loginCurrentBtn;

    @FindBy(id = "remember")
    private WebElement rememberCheckBox;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "error")
    private WebElement wrongName;

    public void inputLogin(String login) {
        loginField.sendKeys(login);
    }
    public void inputPassword(String password) {
        passwordField.sendKeys(password);
    }
    public void clickLoginBtn() {
        loginBtn.click();
    }
    public void clickLoginAnotherBtn() {
        loginAnotherBtn.click();
    }
    public void clickLoginCurrentBtn() {
        loginCurrentBtn.click();
    }
    public void clickRememberCheckBox() {
        rememberCheckBox.click();
    }
    public void wrongInput() {
        //Incorrect username/password notification
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("error")));
    }
}

